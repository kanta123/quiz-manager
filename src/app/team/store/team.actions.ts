import { createAction, props } from '@ngrx/store';
import { Team } from 'src/app/models/team/team';

export const addTeam = createAction(
  '[Add New Team] Add new team to database',
  props<{ team: Team }>()
);

export const teamAdded = createAction(
  '[Add New Team Effect] New team added to database',
  props<{ team: Team }>()
);
