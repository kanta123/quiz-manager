import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TeamService } from 'src/app/service/team.service';
import { TeamActions } from './action-types';
import { concatMap, map } from 'rxjs/operators';
import { teamAdded } from './team.actions';

@Injectable()
export class TeamEffects {
    constructor(
        private actions$: Actions,
        private teamService: TeamService) { }

    addTeam$ = createEffect(
        () => this.actions$
            .pipe(
                ofType(TeamActions.addTeam),
                concatMap(action => this.teamService.addTeam(action.team.name)),
                map(team => teamAdded({ team }))
            )
    );
}
