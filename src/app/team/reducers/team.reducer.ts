import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Team } from '../../models/team/team';
import { TeamActions } from '../store/action-types';
import { createReducer, on } from '@ngrx/store';

export const teamsFeatureKey = 'teams';

export interface TeamState extends EntityState<Team> {
  // additional entities state properties
}

export const adapter: EntityAdapter<Team> = createEntityAdapter<Team>();

export const initialTeamState: TeamState = adapter.getInitialState({
  // additional entity state properties
});


export function teamReducer(state, action) {

  return _teamReducer(state, action);

}

export const _teamReducer = createReducer(
  initialTeamState,

  on(TeamActions.teamAdded, (state, action) =>
    adapter.addOne(action.team, state)
  )
);


export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
