import { DataSource } from '@angular/cdk/table';
import { TeamResponse } from 'src/app/models/team/team.response';
import { CollectionViewer } from '@angular/cdk/collections';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { TeamService } from 'src/app/service/team.service';
import { catchError, finalize } from 'rxjs/operators';
import { Paged } from 'src/app/models/common/paged.response';

export class TeamListDataSource implements DataSource<TeamResponse> {

    private teamSubject = new BehaviorSubject<TeamResponse[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();
    public total = 0;

    constructor(private teamService: TeamService) { }

    connect(collectionViewer: CollectionViewer): Observable<TeamResponse[]> {
        return this.teamSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.teamSubject.complete();
        this.loadingSubject.complete();
    }

    load(page: number, limit: number, name: string) {

        this.loadingSubject.next(true);

        this.teamService.getTeams(page, limit, name)
            .pipe(
                catchError(() => of<TeamResponse[]>([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe(
                (response: Paged<TeamResponse[]>) => {
                    this.total = response.total;
                    this.teamSubject.next(response.data);
                }
            );

    }
}
