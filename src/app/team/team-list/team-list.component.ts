import { Component, OnInit, ViewChild, AfterViewInit, ElementRef, OnDestroy } from '@angular/core';
import { TeamService } from 'src/app/service/team.service';
import { TeamListDataSource } from './team.list.datasource';
import { MatPaginator } from '@angular/material/paginator';
import { tap, debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { fromEvent, Subscription } from 'rxjs';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.css']
})
export class TeamListComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild(MatPaginator, { static: false }) private paginator: MatPaginator;
  @ViewChild('search', { static: false }) private search: ElementRef;

  private searchSubscription: Subscription;
  private paginationSubscription: Subscription;
  public columns = ['id', 'name', 'delete'];

  public dataSource: TeamListDataSource;
  private page = 1;
  private limit = 5;
  public pageSize = 10;

  constructor(private teamService: TeamService) { }

  removeTeam(teamId: number): void {
    this.teamService.deleteTeam(teamId).subscribe(id => {
      this.dataSource.load(this.paginator.pageIndex + 1, this.paginator.pageSize, null);
    });
  }

  ngOnInit() {
    this.dataSource = new TeamListDataSource(this.teamService);
    this.dataSource.load(this.page, this.limit, '');
  }

  ngAfterViewInit() {
    this.paginationSubscription = this.paginator.page
      .pipe(
        tap(() => this.dataSource.load(this.paginator.pageIndex + 1, this.paginator.pageSize, ''))
      )
      .subscribe();

    this.searchSubscription = fromEvent<any>(this.search.nativeElement, 'keyup')
      .pipe(
        map(event => event.target.value),
        debounceTime(400),
        distinctUntilChanged(),
        tap((value) => {
          this.dataSource.load(1, 10, value);
        })
      )
      .subscribe();
  }

  ngOnDestroy() {
    this.paginationSubscription.unsubscribe();
    this.searchSubscription.unsubscribe();
  }
}
