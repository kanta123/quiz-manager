import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeamRoutingModule } from './team-routing.module';
import { TeamListComponent } from './team-list/team-list.component';

// Material
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatInputModule } from '@angular/material/input';
import { StoreModule } from '@ngrx/store';
import * as fromTeam from './reducers/team.reducer';
import { AuthGuardService } from '../auth/guards/auth-guard.service';

@NgModule({
  declarations: [TeamListComponent],
  imports: [
    CommonModule,
    TeamRoutingModule,

    // Material
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatInputModule,

    // Store
    StoreModule.forFeature(fromTeam.teamsFeatureKey, fromTeam.teamReducer),
  ],
  providers: [AuthGuardService],
})
export class TeamModule {}
