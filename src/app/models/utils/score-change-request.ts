export interface ScoreChangeRequest {
  teamId: number;
  quizId: number;
}
