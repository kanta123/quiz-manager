export interface TeamResponse {
    id: number;
    name: string;
}
