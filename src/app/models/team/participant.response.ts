import { Participant } from './participant';

export class ParticipantResponse {
  quizId: number;
  id: number;
  name: string;
  score: number;
  watcherId: number;
}
