import { Team } from './team';

export class Participant extends Team {
  score: number;
  watcherId: number;

  constructor(team: Team) {
    super(team.id, team.name);
    this.score = 0;
  }
}
