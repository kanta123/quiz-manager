export interface CreateQuizRequest {
    name: string;
    startTime: number;
    participantIds: number[];
}
