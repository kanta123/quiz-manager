import { Participant } from '../team/participant';

export class Quiz {
    id: number;
    name: string;
    startTime: number;
    participants: Participant[];
}
