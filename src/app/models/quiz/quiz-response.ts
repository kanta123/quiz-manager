import { ParticipantResponse } from '../team/participant.response';
import { Participant } from '../team/participant';

export interface QuizResponse {
  id: number;
  name: string;
  startTime: number;
  participants: ParticipantResponse[];
}
