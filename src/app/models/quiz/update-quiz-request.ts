import { Dictionary } from '@ngrx/entity';

export interface UpdateQuizRequest {
  name: string;
  startTime: number;
  participantIds: number[];
  participantWatcher: { participantId: number; watcherId: number }[];
}
