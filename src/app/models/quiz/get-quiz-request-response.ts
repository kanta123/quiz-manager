import { Quiz } from './quiz';

export interface GetQuizRequestResponse {
    data: Quiz[];
    limit: number;
    page: number;
    total: number;

}

