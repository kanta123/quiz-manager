export interface Paged<T> {
    data: T;
    page: number;
    total: number;
    limit: number;
}
