export interface WsRequest<T> {
  event: string;
  data: T;
}
