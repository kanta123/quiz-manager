import { Component, OnInit } from '@angular/core';
import { WakeUpService } from './service/wake-up.service';
import { AuthService } from './service/auth.service';
import { User } from './models/user/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  public isLoggedIn$ = this.authService.isLoggedIn$;

  public user: User;

  constructor(
    private wakeUpService: WakeUpService,
    private authService: AuthService,
  ) {
    authService.user$.subscribe(user => (this.user = user));
  }

  ngOnInit() {
    this.wakeUpService.wakeUp().subscribe();
  }

  logout() {
    this.authService.logout().subscribe();
  }
}
