import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { TeamResponse } from '../models/team/team.response';
import { Paged } from '../models/common/paged.response';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TeamService {
  private TEAM_URL = `${environment.api}/team`;

  constructor(private http: HttpClient) {}

  getTeams(
    page: number,
    limit: number,
    name?: string,
  ): Observable<Paged<TeamResponse[]>> {
    return this.http.get<Paged<TeamResponse[]>>(this.TEAM_URL, {
      params: new HttpParams()
        .set('page', page.toString())
        .set('limit', limit.toString())
        .set('name', name),
    });
  }

  addTeam(name: string): Observable<TeamResponse> {
    return this.http.post<TeamResponse>(this.TEAM_URL, { name });
  }

  deleteTeam(id: number): Observable<number> {
    return this.http.delete<number>(this.TEAM_URL + `/${id}`);
  }
}
