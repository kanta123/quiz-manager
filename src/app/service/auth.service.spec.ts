import { TestBed } from '@angular/core/testing';
import { AuthService } from './auth.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { UserResponse } from '../models/user/user-response.model';
import { User } from '../models/user/user.model';

describe('Auth Service:', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthService],
      imports: [HttpClientTestingModule, RouterTestingModule],
    });
  });
  describe('', () => {
    function setup() {
      const authService: AuthService = TestBed.get(AuthService);
      const httpTestingController: HttpTestingController = TestBed.get(
        HttpTestingController,
      );
      return { authService, httpTestingController };
    }

    it('should be created', () => {
      const authService = setup().authService;
      const httpTestingController = setup().httpTestingController;

      expect(authService).toBeTruthy();

      const req = httpTestingController.expectOne('http://localhost:3000/user');

      expect(req.request.method).toBe('GET');

      expect(req.request.withCredentials).toBe(true);
    });

    it(`#login should send a request with the withCredentials flag set to true
    and username and password in the body of the request`, () => {
      const authService = setup().authService;
      const httpTestingController = setup().httpTestingController;

      const subjectValue = authService.subject.getValue();
      authService.login('Test', 'test').subscribe(res => {
        expect(res).toEqual({ id: 1, username: 'Test' } as UserResponse);
        const emittedValue = authService.subject.getValue();
        expect(emittedValue).toEqual({ id: 1, username: 'Test' } as User);
      });

      const reqStartUp = httpTestingController.expectOne(
        'http://localhost:3000/user',
      );
      const req = httpTestingController.expectOne(
        'http://localhost:3000/user/login',
      );

      expect(reqStartUp.request.method).toBe('GET');

      expect(req.request.method).toBe('POST');
      expect(req.request.withCredentials).toBe(true);
      expect(req.request.body).toEqual({
        username: 'Test',
        password: 'test',
      });

      req.flush({ id: 1, username: 'Test' } as UserResponse);
    });

    it('#logout should make a call to the correct endpoint', () => {
      const authService = setup().authService;
      const httpTestingController = setup().httpTestingController;

      authService.logout().subscribe(res => {
        expect(res).toEqual('');
        expect(authService.subject.getValue()).toEqual({
          id: undefined,
          username: '',
        } as User);
      });

      const reqStartUp = httpTestingController.expectOne(
        'http://localhost:3000/user',
      );
      const req = httpTestingController.expectOne(
        'http://localhost:3000/user/logout',
      );

      expect(req.request.method).toBe('POST');
      expect(req.request.withCredentials).toBe(true);
      req.flush('');
    });

    it(`#signup should send a request with the withCredentials flag set to true
    and username and password in the body of the request`, () => {
      const authService = setup().authService;
      const httpTestingController = setup().httpTestingController;
      const expectedSignupMethodResponse: UserResponse = {
        id: 1,
        username: 'Test',
      };

      authService.signup('Test', 'test').subscribe(res => {
        expect(res).toEqual(expectedSignupMethodResponse);
        const subjectValue = authService.subject.getValue();
        expect(subjectValue).toEqual(expectedSignupMethodResponse);
      });

      const request = httpTestingController.match('http://localhost:3000/user');
      const req = request.find(r => r.request.method === 'POST');
      expect(req.request.method).toBe('POST');
      expect(req.request.body).toEqual({ username: 'Test', password: 'test' });
      expect(req.request.withCredentials).toBe(true);
      req.flush({ id: 1, username: 'Test' } as UserResponse);
    });

    afterEach(() => {
      const httpTestingController = setup().httpTestingController;
      httpTestingController.verify();
    });
  });
});
