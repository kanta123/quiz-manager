import { TestBed, async } from '@angular/core/testing';

import { WakeUpService } from './wake-up.service';
import { Observable } from 'rxjs';

let httpClientSpy: { get: jasmine.Spy };
let wakeUpService: WakeUpService;

describe('Wake Up Service', async () => {
  beforeEach(() => {
    TestBed.configureTestingModule({});

    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    wakeUpService = new WakeUpService(httpClientSpy as any);
  });

  it('should be created', () => expect(wakeUpService).toBeTruthy());

  it('should send an empty JSON to the api and return a string observable', () => {
    const expectedStringObservable: Observable<string> = new Observable<
      string
    >();

    httpClientSpy.get.and.returnValue(expectedStringObservable);

    wakeUpService.wakeUp().subscribe(res => expect(res).toEqual(''), fail);

    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });
});
