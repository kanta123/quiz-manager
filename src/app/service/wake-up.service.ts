import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class WakeUpService {

    private PING_URL = `${environment.api}/wake-up`;

    constructor(private http: HttpClient) { }

    wakeUp(): Observable<string> {
        return this.http.get<string>(this.PING_URL);
    }

}
