import { QuizService } from './quiz.service';
import { TestBed } from '@angular/core/testing';
import { QuizResponse } from '../models/quiz/quiz-response';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { Quiz } from '../models/quiz/quiz';
import { UpdateQuizRequest } from '../models/quiz/update-quiz-request';
import { GetQuizRequestResponse } from '../models/quiz/get-quiz-request-response';
import { HttpParams } from '@angular/common/http';
import { ParticipantResponse } from '../models/team/participant.response';
import { CreateQuizRequest } from '../models/quiz/create-quiz-request';

describe('Quiz Service:', () => {
  beforeEach(async () => {
    TestBed.configureTestingModule({
      providers: [QuizService],
      imports: [HttpClientTestingModule],
    });
  });
  describe('', () => {
    function setup() {
      const quizService = TestBed.get(QuizService);
      const httpTestingController = TestBed.get(HttpTestingController);
      return { quizService, httpTestingController };
    }
    it('should be created', () => {
      const quizService: QuizService = setup().quizService;
      expect(quizService).toBeTruthy();
    });

    it('#getQuiz should send a number and return a QuizResponse object', () => {
      const quizService: QuizService = setup().quizService;
      const httpTestingController: HttpTestingController = setup()
        .httpTestingController;

      const expectedGetQuizResponse: QuizResponse = {
        id: 1,
        name: 'Test Quiz',
        startTime: 0,
        participants: [
          { quizId: 1, id: 1, name: 'Test Participant', score: 0 },
        ],
      };
      quizService
        .getQuiz(1)
        .subscribe(response =>
          expect(response).toEqual(expectedGetQuizResponse),
        );

      const req = httpTestingController.expectOne(
        'http://localhost:3000/quiz/1',
      );

      expect(req.request.method).toBe('GET');
      req.flush({
        id: expectedGetQuizResponse.id,
        name: expectedGetQuizResponse.name,
        startTime: expectedGetQuizResponse.startTime,
        participants: expectedGetQuizResponse.participants,
      } as Quiz);
    });

    it('#updateQuiz should parse the input correctly and send it in the request body', () => {
      const quizService: QuizService = setup().quizService;
      const httpTestingController: HttpTestingController = setup()
        .httpTestingController;

      const expectedUpdateQuizMethodResponse: Quiz = {
        id: 1,
        name: 'Test Quiz',
        participants: [{ id: 1, name: 'Test Team', score: 0 }],
        startTime: 0,
      };

      const expectedQuizToQuizRequestMap: UpdateQuizRequest = {
        name: expectedUpdateQuizMethodResponse.name,
        startTime: expectedUpdateQuizMethodResponse.startTime,
        participantIds: expectedUpdateQuizMethodResponse.participants.map(
          participant => participant.id,
        ),
      };

      quizService
        .updateQuiz(expectedUpdateQuizMethodResponse)
        .subscribe(res =>
          expect(res).toEqual(expectedUpdateQuizMethodResponse),
        );

      const req = httpTestingController.expectOne(
        'http://localhost:3000/quiz/1',
      );

      expect(req.request.method).toBe('PUT');

      expect(req.request.body).toEqual(expectedQuizToQuizRequestMap);

      req.flush({
        id: expectedUpdateQuizMethodResponse.id,
        name: expectedUpdateQuizMethodResponse.name,
        startTime: expectedUpdateQuizMethodResponse.startTime,
        participants: expectedUpdateQuizMethodResponse.participants,
      } as Quiz);
    });

    it('#getUpcomingQuizzes should map the method parameters and send it as the request parameters', () => {
      const quizService: QuizService = setup().quizService;
      const httpTestingController: HttpTestingController = setup()
        .httpTestingController;

      const expectedGetUpcomingQuizzesMethodResponse: GetQuizRequestResponse = {
        data: [
          {
            id: 1,
            name: 'Test Quiz',
            participants: [{ id: 1, name: 'Test Participant', score: 0 }],
            startTime: 0,
          },
        ],
        limit: 1,
        page: 1,
        total: 1,
      };

      let expectedGetUpcomingQuizzesRequestParams = new HttpParams();
      expectedGetUpcomingQuizzesRequestParams = expectedGetUpcomingQuizzesRequestParams.append(
        'page',
        '1',
      );
      expectedGetUpcomingQuizzesRequestParams = expectedGetUpcomingQuizzesRequestParams.append(
        'limit',
        '1',
      );
      expectedGetUpcomingQuizzesRequestParams = expectedGetUpcomingQuizzesRequestParams.append(
        'startTime',
        '0',
      );
      expectedGetUpcomingQuizzesRequestParams = expectedGetUpcomingQuizzesRequestParams.append(
        'isFuture',
        'true',
      );

      quizService
        .getUpcomingQuizzes(1, 1, 0, true)
        .subscribe(res =>
          expect(res).toEqual(expectedGetUpcomingQuizzesMethodResponse),
        );

      const req = httpTestingController.expectOne(
        'http://localhost:3000/quiz?page=1&limit=1&startTime=0&isFuture=true',
      );
      expect(req.request.method).toBe('GET');

      expect(req.request.params.toString()).toEqual(
        expectedGetUpcomingQuizzesRequestParams.toString(),
      );

      req.flush({
        data: expectedGetUpcomingQuizzesMethodResponse.data,
        limit: expectedGetUpcomingQuizzesMethodResponse.limit,
        page: expectedGetUpcomingQuizzesMethodResponse.page,
        total: expectedGetUpcomingQuizzesMethodResponse.total,
      } as GetQuizRequestResponse);
    });

    it('#incrementScore should map the method params to http params correctly', () => {
      const quizService: QuizService = setup().quizService;
      const httpTestingController: HttpTestingController = setup()
        .httpTestingController;
      const expectedIncrementScoreMethodResponse: ParticipantResponse = {
        quizId: 1,
        id: 1,
        name: 'Test Name',
        score: 1,
      };

      quizService
        .incrementScore(1, 1)
        .subscribe(res =>
          expect(res).toEqual(expectedIncrementScoreMethodResponse),
        );

      const req = httpTestingController.expectOne(
        'http://localhost:3000/quiz/1/participants/1/increment',
      );

      expect(req.request.method).toBe('PUT');
      expect(req.request.body).toEqual('');

      req.flush({
        id: expectedIncrementScoreMethodResponse.id,
        name: expectedIncrementScoreMethodResponse.name,
        score: expectedIncrementScoreMethodResponse.score,
      } as ParticipantResponse);
    });

    it('#decrementScore should map the method params to http params correctly', () => {
      const quizService: QuizService = setup().quizService;
      const httpTestingController: HttpTestingController = setup()
        .httpTestingController;
      const expectedIncrementScoreMethodResponse: ParticipantResponse = {
        quizId: 1,
        id: 1,
        name: 'Test Name',
        score: 1,
      };

      quizService
        .decrementScore(1, 1)
        .subscribe(res =>
          expect(res).toEqual(expectedIncrementScoreMethodResponse),
        );

      const req = httpTestingController.expectOne(
        'http://localhost:3000/quiz/1/participants/1/decrement',
      );

      expect(req.request.method).toBe('PUT');
      expect(req.request.body).toEqual('');

      req.flush({
        id: expectedIncrementScoreMethodResponse.id,
        name: expectedIncrementScoreMethodResponse.name,
        score: expectedIncrementScoreMethodResponse.score,
      } as ParticipantResponse);
    });

    it('#addQuiz should correctly map input params and send them in the request body', () => {
      const quizService: QuizService = setup().quizService;
      const httpTestingController: HttpTestingController = setup()
        .httpTestingController;

      const addQuizInputParam: Quiz = {
        id: 1,
        name: 'Test Quiz',
        participants: [{ id: 1, name: 'Test Participant', score: 0 }],
        startTime: 0,
      };

      quizService
        .addQuiz(addQuizInputParam)
        .subscribe(res => expect(res).toEqual(addQuizInputParam));

      const req = httpTestingController.expectOne('http://localhost:3000/quiz');

      expect(req.request.method).toBe('POST');
      expect(req.request.body).toEqual({
        name: addQuizInputParam.name,
        participantIds: addQuizInputParam.participants.map(part => part.id),
        startTime: addQuizInputParam.startTime,
      } as CreateQuizRequest);

      req.flush({
        id: addQuizInputParam.id,
        name: addQuizInputParam.name,
        participants: addQuizInputParam.participants,
        startTime: addQuizInputParam.startTime,
      } as Quiz);
    });

    it('#deleteQuiz should send a valid request to the backend', () => {
      const quizService: QuizService = setup().quizService;
      const httpTestingController: HttpTestingController = setup()
        .httpTestingController;
      const expectedDeleteQuizMethodResponse = 1;

      quizService.deleteQuiz(1).subscribe(res => expect(res).toEqual(1));

      const req = httpTestingController.expectOne(
        'http://localhost:3000/quiz/1',
      );

      expect(req.request.method).toBe('DELETE');

      req.flush(1);
    });

    it('#removeParticipant should send a request to a valid url', () => {
      const quizService: QuizService = setup().quizService;
      const httpTestingController: HttpTestingController = setup()
        .httpTestingController;

      const expectedRemoveParticipantMethodResponse = 1;

      quizService
        .removeParticipant(1, 1)
        .subscribe(res => expect(res).toEqual(1));

      const req = httpTestingController.expectOne(
        'http://localhost:3000/quiz/1/participants/1',
      );

      expect(req.request.method).toBe('DELETE');

      req.flush(1);
    });

    afterEach(() => {
      const httpTestingController: HttpTestingController = setup()
        .httpTestingController;
      httpTestingController.verify();
    });
  });
});
