import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { UserResponse } from '../models/user/user-response.model';
import { Observable, BehaviorSubject } from 'rxjs';
import { User } from '../models/user/user.model';
import { tap, map, shareReplay, filter } from 'rxjs/operators';
import { Router } from '@angular/router';

const API_URL = `${environment.api}`;

const ANONYMOUS: User = { id: undefined, username: '' };

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  public subject = new BehaviorSubject<User>(ANONYMOUS);

  public user$ = this.subject.asObservable().pipe(filter(user => !!user));

  public isLoggedIn$ = this.user$.pipe(map(user => !!user.id));

  constructor(private http: HttpClient, private router: Router) {
    http
      .get<User>(`${API_URL}/user`, { withCredentials: true })
      .subscribe(user => {
        this.subject.next(!!user ? user : ANONYMOUS);
      });
  }

  login(username: string, password: string) {
    return this.http
      .post<UserResponse>(
        `${API_URL}/user/login`,
        { username, password },
        { withCredentials: true },
      )
      .pipe(
        shareReplay(),
        tap(user => {
          this.subject.next(user);
          this.router.navigateByUrl('/');
        }),
      );
  }

  signup(username: string, password: string): Observable<UserResponse> {
    return this.http
      .post<UserResponse>(
        `${API_URL}/user`,
        { username, password },
        { withCredentials: true },
      )
      .pipe(
        shareReplay(),
        tap(user => {
          this.subject.next(user);
          this.router.navigateByUrl('/');
        }),
      );
  }

  logout(): Observable<any> {
    return this.http
      .post(`${API_URL}/user/logout`, null, { withCredentials: true })
      .pipe(
        tap(() => {
          this.subject.next(ANONYMOUS);
          this.router.navigateByUrl('/login');
        }),
      );
  }
}
