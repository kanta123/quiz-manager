import { Injectable } from '@angular/core';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { WsRequest } from '../models/common/ws-request';
import { ScoreChangeRequest } from '../models/utils/score-change-request';
import { Observable } from 'rxjs';
import { ParticipantResponse } from '../models/team/participant.response';
import { AppState } from '../reducers';
import { Store } from '@ngrx/store';
import {
  pointsIncremented,
  pointsDecremented,
} from '../quiz/store/quiz.actions';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ParticipantWebsocketService {
  constructor(private store: Store<AppState>) {
    this.participantSocket$ = webSocket({
      url: 'wss://quiz-management.herokuapp.com/',
    });

    this.incrementChannel$ = this.participantSocket$.multiplex(
      () => ({ event: 'increment', data: null }),
      () => ({ unsubscribe: 'unsubscribeIncrement' }),
      message => {
        console.log(message);
        return message.event === 'increment';
      },
    );

    this.decrementChannel$ = this.participantSocket$.multiplex(
      () => ({ event: 'decrement', data: null }),
      () => ({ unsubscribe: 'unsubscribeDecrement' }),
      message => {
        console.log(message);
        return message.event === 'decrement';
      },
    );

    this.incrementChannel$.subscribe(response => {
      this.store.dispatch(
        pointsIncremented({
          participantId: response.data.id,
          quizId: response.data.quizId,
          score: response.data.score,
        }),
      );
    });

    this.decrementChannel$.subscribe(response =>
      this.store.dispatch(
        pointsDecremented({
          participantId: response.data.id,
          quizId: response.data.quizId,
          score: response.data.score,
        }),
      ),
    );
  }

  public participantSocket$: WebSocketSubject<
    WsRequest<ScoreChangeRequest | ParticipantResponse>
  >;

  public decrementChannel$: Observable<WsRequest<ParticipantResponse>>;

  public incrementChannel$: Observable<WsRequest<ParticipantResponse>>;

  incrementParticipantScore(teamId: number, quizId: number) {
    this.participantSocket$.next({
      event: 'increment',
      data: { teamId, quizId },
    });
  }

  decerementParticipantScore(teamId: number, quizId: number) {
    this.participantSocket$.next({
      event: 'decrement',
      data: { teamId, quizId },
    });
  }
}
