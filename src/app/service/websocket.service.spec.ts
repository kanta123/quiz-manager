import { TestBed } from '@angular/core/testing';

import { ParticipantWebsocketService } from './websocket.service';

describe('WebsocketService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ParticipantWebsocketService = TestBed.get(
      ParticipantWebsocketService,
    );
    expect(service).toBeTruthy();
  });
});
