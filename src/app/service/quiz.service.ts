import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Quiz } from '../models/quiz/quiz';
import { HttpClient, HttpParams } from '@angular/common/http';
import { CreateQuizRequest } from '../models/quiz/create-quiz-request';
import { GetQuizRequestResponse } from '../models/quiz/get-quiz-request-response';
import { environment } from 'src/environments/environment';
import { ParticipantResponse } from '../models/team/participant.response';
import { UpdateQuizRequest } from '../models/quiz/update-quiz-request';
import { QuizResponse } from '../models/quiz/quiz-response';
import { map } from 'rxjs/operators';
import { Participant } from '../models/team/participant';

@Injectable({
  providedIn: 'root',
})
export class QuizService {
  private QUIZ_URL = `${environment.api}/quiz`;

  constructor(private http: HttpClient) {}

  getQuiz(id: number): Observable<Quiz> {
    return this.http.get<QuizResponse>(`${this.QUIZ_URL}/${id}`).pipe(
      map(res => {
        return {
          ...res,
          participants: res.participants.map(partRes => {
            const p = new Participant({
              id: partRes.id,
              name: partRes.name,
            });
            p.score = partRes.score;
            p.id = partRes.id;
            p.watcherId = partRes.watcherId;
            return p;
          }),
        };
      }),
    );
  }

  updateQuiz(quiz: Quiz): Observable<Quiz> {
    console.log(quiz.participants);
    const participantWatcher: {
      participantId: number;
      watcherId: number;
    }[] = [];
    quiz.participants.forEach(p => {
      participantWatcher.push({ participantId: p.id, watcherId: p.watcherId });
    });

    const quizUpdateRequest: UpdateQuizRequest = {
      name: quiz.name,
      startTime: quiz.startTime,
      participantIds: quiz.participants.map(participant => participant.id),
      participantWatcher,
    };

    console.log(participantWatcher);
    return this.http.put<Quiz>(
      `${this.QUIZ_URL}/${quiz.id}`,
      quizUpdateRequest,
      { withCredentials: true },
    );
  }

  getUpcomingQuizzes(
    pageNumber: number,
    limitNumber: number,
    startTimeNumber: number,
    isFuture: boolean,
  ): Observable<GetQuizRequestResponse> {
    let queryParams = new HttpParams();
    queryParams = queryParams.append('page', pageNumber.toString());
    queryParams = queryParams.append('limit', limitNumber.toString());
    queryParams = queryParams.append('startTime', startTimeNumber.toString());
    queryParams = queryParams.append('isFuture', isFuture.toString());
    return this.http.get<GetQuizRequestResponse>(this.QUIZ_URL, {
      params: queryParams,
    });
  }

  incrementScore(quizId: number, teamId: number): Observable<Participant> {
    return this.http
      .put<ParticipantResponse>(
        `${this.QUIZ_URL}/${quizId}/participants/${teamId}/increment`,
        '',
      )
      .pipe(
        map(res => {
          const p = {
            id: res.id,
            name: res.name,
            score: res.score,
            watcherId: res.watcherId,
          };
          return p;
        }),
      );
  }

  decrementScore(quizId: number, teamId: number): Observable<Participant> {
    return this.http
      .put<ParticipantResponse>(
        `${this.QUIZ_URL}/${quizId}/participants/${teamId}/decrement`,
        '',
      )
      .pipe(
        map(res => {
          const p = {
            id: res.id,
            name: res.name,
            score: res.score,
            watcherId: res.watcherId,
          };
          return p;
        }),
      );
  }

  addQuiz(quiz: Quiz): Observable<Quiz> {
    const newQuiz: CreateQuizRequest = {
      name: quiz.name,
      startTime: quiz.startTime,
      participantIds: quiz.participants.map(participant => participant.id),
    };
    return this.http.post<Quiz>(this.QUIZ_URL, newQuiz, {
      withCredentials: true,
    });
  }

  deleteQuiz(id: number): Observable<number> {
    return this.http.delete<number>(`${this.QUIZ_URL}/${id}`);
  }

  removeParticipant(quizId: number, participantId: number): Observable<number> {
    return this.http.delete<number>(
      `${this.QUIZ_URL}/${quizId}/participants/${participantId}`,
    );
  }
}
