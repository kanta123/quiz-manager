import { TestBed, inject } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

import { TeamService } from './team.service';
import { TeamResponse } from '../models/team/team.response';
import { Paged } from '../models/common/paged.response';

describe('TeamService:', () => {
  beforeEach(async () => {
    TestBed.configureTestingModule({
      providers: [TeamService],
      imports: [HttpClientTestingModule],
    });
  });

  describe('', () => {
    function setup() {
      const teamService = TestBed.get(TeamService);

      const httpTestingController = TestBed.get(HttpTestingController);

      return { teamService, httpTestingController };
    }

    it('should be created', () => {
      const app = setup();
      const teamService = app.teamService;
      expect(teamService).toBeTruthy();
    });

    it('#getTeams should return a paged array of TeamResponse objects', () => {
      const app = setup();
      const teamService: TeamService = app.teamService;
      const httpTestingController: HttpTestingController =
        app.httpTestingController;

      const expectedResponseFromGetTeamsMethod: Paged<TeamResponse[]> = {
        data: [{ id: 1, name: 'Team' }],
        limit: 1,
        page: 1,
        total: 1,
      };

      teamService
        .getTeams(1, 1, 'Team')
        .subscribe(res =>
          expect(res).toEqual(expectedResponseFromGetTeamsMethod),
        );
      const req = httpTestingController.expectOne(
        'http://localhost:3000/team?page=1&limit=1&name=Team',
      );

      expect(req.request.method).toBe('GET');

      req.flush({
        data: expectedResponseFromGetTeamsMethod.data,
        limit: expectedResponseFromGetTeamsMethod.limit,
        total: expectedResponseFromGetTeamsMethod.total,
        page: expectedResponseFromGetTeamsMethod.page,
      });
    });

    it('#addTeam should send a string and return an TeamResponse object with the property "name" equal to the sent string', () => {
      const app = setup();
      const teamService: TeamService = app.teamService;
      const httpTestingController: HttpTestingController =
        app.httpTestingController;
      const expectedResponseFromAddTeamMethod: TeamResponse = {
        id: 1,
        name: 'Test Name',
      };

      teamService
        .addTeam('Test Name')
        .subscribe(res =>
          expect(res).toEqual(expectedResponseFromAddTeamMethod),
        );

      const req = httpTestingController.expectOne('http://localhost:3000/team');

      expect(req.request.method).toBe('POST');

      req.flush({
        id: expectedResponseFromAddTeamMethod.id,
        name: expectedResponseFromAddTeamMethod.name,
      });
    });

    it('#deleteTeam should send an id of the team to delete and return the same id', () => {
      const app = setup();
      const teamService: TeamService = app.teamService;
      const httpTestingController: HttpTestingController =
        app.httpTestingController;
      const expectedResponseFromDeleteTeamMethod = 1;

      teamService
        .deleteTeam(1)
        .subscribe(res =>
          expect(res).toEqual(expectedResponseFromDeleteTeamMethod),
        );

      const req = httpTestingController.expectOne(
        'http://localhost:3000/team/1',
      );

      expect(req.request.method).toBe('DELETE');

      req.flush(expectedResponseFromDeleteTeamMethod);
    });

    afterEach(() => {
      const httpTestingController: HttpTestingController = setup()
        .httpTestingController;
      httpTestingController.verify();
    });
  });
});
