import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouterState, StoreRouterConnectingModule } from '@ngrx/router-store';
import { AuthGuardService } from './auth/guards/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: '/quiz',
        pathMatch: 'full',
      },
      {
        path: 'quiz',
        loadChildren: () =>
          import('./quiz/quiz.module').then(module => module.QuizModule),
      },
      {
        path: 'team',
        loadChildren: () =>
          import('./team/team.module').then(module => module.TeamModule),
      },
    ],
    canActivate: [AuthGuardService],
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./auth/auth.module').then(module => module.AuthModule),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    StoreRouterConnectingModule.forRoot({
      stateKey: 'router',
      routerState: RouterState.Minimal,
    }),
  ],
  exports: [RouterModule],
  providers: [AuthGuardService],
})
export class AppRoutingModule {}
