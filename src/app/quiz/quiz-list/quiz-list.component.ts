import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Quiz } from 'src/app/models/quiz/quiz';
import { Router } from '@angular/router';
import { AppState } from 'src/app/reducers';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import {
  selectPastQuizzes,
  selectUpcomingQuizzes,
} from '../store/quiz.selectors';
import {
  loadUpcomingQuizzes,
  loadPastQuizzes,
  deleteQuiz,
  getQuiz,
} from '../store/quiz.actions';

@Component({
  selector: 'app-quiz-list',
  templateUrl: './quiz-list.component.html',
  styleUrls: ['./quiz-list.component.css'],
})
export class QuizListComponent implements OnInit {
  public upcomingQuizzesDataSource: MatTableDataSource<
    Quiz
  > = new MatTableDataSource();

  public pastQuizzesDataSource: MatTableDataSource<
    Quiz
  > = new MatTableDataSource();

  public upcomingQuizzesStartTime: number = new Date().setHours(0, 0, 0, 0);

  public loading = true;

  public upcomingQuizzes$: Observable<Quiz[]>;

  public pastQuizzes$: Observable<Quiz[]>;

  public quizzes: Observable<Quiz[]>;

  public loaderMode = 'query';

  constructor(private router: Router, private store: Store<AppState>) {}

  onQuizSelect(id: number): void {
    this.store.dispatch(getQuiz({ quizId: id }));
    this.router.navigateByUrl('/quiz/' + id + '/info');
  }

  ngOnInit() {
    this.reload();
  }

  reload() {
    this.store.pipe(select(selectPastQuizzes)).subscribe(s => {
      this.pastQuizzesDataSource = new MatTableDataSource(s);
    });
  }

  tabSwitchFuture($event): void {
    if ($event.tab.textLabel === 'Past') {
      this.store.dispatch(loadPastQuizzes());
      this.store
        .pipe(select(selectPastQuizzes))
        .subscribe(
          s => (this.pastQuizzesDataSource = new MatTableDataSource(s)),
        );
    } else if ($event.tab.textLabel === 'Future') {
      this.store.dispatch(loadUpcomingQuizzes());
      this.store
        .pipe(select(selectUpcomingQuizzes))
        .subscribe(
          s => (this.upcomingQuizzesDataSource = new MatTableDataSource(s)),
        );
    }
  }

  deleteOldQuiz(quiz: Quiz): void {
    this.store.dispatch(deleteQuiz({ quiz }));
  }

  deleteNewQuiz(quiz: Quiz): void {
    this.store.dispatch(deleteQuiz({ quiz }));
  }
}
