import { MetaReducer, createReducer, on } from '@ngrx/store';
import { environment } from '../../../environments/environment';
import { EntityState, createEntityAdapter, Update } from '@ngrx/entity';
import { Quiz } from 'src/app/models/quiz/quiz';
import { QuizActions } from '../store/action-types';
import { NAMED_ENTITIES } from '@angular/compiler';

export const quizFeatureKey = 'quiz';

export interface QuizState extends EntityState<Quiz> {
  selectedQuizId: number;
  isLoading: boolean;
}

export const adapter = createEntityAdapter<Quiz>();

export const initialQuizState = adapter.getInitialState({
  selectedQuizId: null,
  isLoading: false,
});

export const _quizReducer = createReducer(
  initialQuizState,

  on(QuizActions.upcomingQuizzesLoaded, (state, action) =>
    adapter.addAll(action.quizzes, state),
  ),

  on(QuizActions.pastQuizzesLoaded, (state, action) =>
    adapter.addAll(action.quizzes, { ...state, selectedQuizId: null }),
  ),

  on(QuizActions.quizDeleted, (state, action) =>
    adapter.removeOne(action.id, state),
  ),

  on(QuizActions.quizCreated, (state, action) =>
    adapter.addOne(action.quiz, state),
  ),

  on(QuizActions.gotQuiz, (state, action) =>
    adapter.addOne(action.quiz, {
      ...state,
      isLoading: true,
      selectedQuizId: action.quiz.id,
    }),
  ),

  on(QuizActions.endedLoading, (state, action) => ({
    ...state,
    isLoading: false,
  })),

  on(QuizActions.pointsIncremented, (state, action) => {
    const quiz = state.entities[action.quizId];
    return {
      ...state,
      entities: {
        ...state.entities,
        [quiz.id]: {
          ...quiz,
          participants: quiz.participants.map(participant => {
            if (participant.id === action.participantId) {
              return { ...participant, score: action.score };
            }
            return participant;
          }),
        },
      },
    };
  }),

  on(QuizActions.pointsDecremented, (state, action) => {
    const quiz = state.entities[action.quizId];
    return {
      ...state,
      entities: {
        ...state.entities,
        [quiz.id]: {
          ...quiz,
          participants: quiz.participants.map(participant => {
            if (participant.id === action.participantId) {
              return { ...participant, score: action.score };
            }
            return participant;
          }),
        },
      },
    };
  }),

  on(QuizActions.removeSelectedQuiz, (state, action) => ({
    ...state,
    selectedQuizId: null,
  })),
);

export function quizReducer(state, action) {
  return _quizReducer(state, action);
}

export const { selectAll } = adapter.getSelectors();

export const metaReducers: MetaReducer<QuizState>[] = !environment.production
  ? []
  : [];
