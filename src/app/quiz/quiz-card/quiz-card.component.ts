import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { ParticipantResponse } from 'src/app/models/team/participant.response';
import { Store, select } from '@ngrx/store';
import { AppState } from 'src/app/reducers';
import { selectCurrentQuiz } from '../store/quiz.selectors';
import { map, switchMap } from 'rxjs/operators';
import {
  incrementOneParticipantScore,
  decrementOneParticipantScore,
} from '../store/quiz.actions';
import { Participant } from 'src/app/models/team/participant';
import { Quiz } from 'src/app/models/quiz/quiz';
import { AuthService } from 'src/app/service/auth.service';
import { User } from 'src/app/models/user/user.model';

@Component({
  selector: 'app-quiz-card',
  templateUrl: './quiz-card.component.html',
  styleUrls: ['./quiz-card.component.css'],
})
export class QuizCardComponent implements OnInit {
  public quiz: Quiz;
  public dataSource: MatTableDataSource<Participant> = new MatTableDataSource();
  public isVisible = true;
  public user: User;

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private authService: AuthService,
  ) {}

  ngOnInit() {
    this.authService.user$
      .pipe(
        switchMap(user => {
          this.user = user;
          return this.store.select(selectCurrentQuiz);
        }),
      )
      .subscribe((q: Quiz) => {
        this.quiz = q;
        this.dataSource = new MatTableDataSource(
          q.participants.filter(p => {
            if (p.watcherId === this.user.id) {
              return p;
            }
          }),
        );
      });

    this.store.pipe(select(selectCurrentQuiz)).subscribe((q: Quiz) => {});
  }

  editQuiz(): void {
    this.router.navigateByUrl(`/quiz/${this.quiz.id}/edit`);
  }

  decrementPoints(participant: ParticipantResponse): void {
    this.store.dispatch(
      decrementOneParticipantScore({
        participantID: participant.id,
        quizID: this.quiz.id,
      }),
    );
  }

  incrementPoints(participant: ParticipantResponse): void {
    this.store.dispatch(
      incrementOneParticipantScore({
        participantID: participant.id,
        quizID: this.quiz.id,
      }),
    );
  }
}
