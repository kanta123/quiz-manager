import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { Team } from 'src/app/models/team/team';
import { switchMap, debounceTime } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Participant } from 'src/app/models/team/participant';
import { MatTableDataSource } from '@angular/material/table';
import { TeamService } from 'src/app/service/team.service';
import { ParticipantResponse } from 'src/app/models/team/participant.response';
import { AppState } from 'src/app/reducers';
import { Store, select } from '@ngrx/store';
import {
  createQuiz,
  editQuiz,
  removeParticipantFromQuiz,
} from '../store/quiz.actions';
import { selectCurrentQuiz } from '../store/quiz.selectors';
import { Quiz } from 'src/app/models/quiz/quiz';
import { User } from 'src/app/models/user/user.model';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-quiz-create',
  templateUrl: './quiz-create.component.html',
  styleUrls: ['./quiz-create.component.css'],
})
export class QuizCreateComponent implements OnInit, OnDestroy {
  public firstFormGroup: FormGroup;
  public secondFormGroup: FormGroup;
  public minDate: Date = new Date();
  public quiz: Quiz;
  public team: Team;
  public allTeams: Team[];
  public teams: Team[] = [];
  public dataSource = new MatTableDataSource();
  public isShown = false;
  public isEdit = false;
  public isAlive = true;
  public user: User;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private teamService: TeamService,
    private store: Store<AppState>,
    private authService: AuthService,
  ) {}

  onSubmitQuiz(): void {
    this.quiz.name = this.firstFormGroup.controls.nameControl.value;

    this.quiz.startTime = this.firstFormGroup.controls.dateControl.value.getTime();
  }

  onSubmitTeam(): void {
    this.teams.forEach(element =>
      this.quiz.participants.push(new Participant(element)),
    );

    if (!this.isEdit) {
      this.store.dispatch(createQuiz({ quiz: this.quiz }));
    } else {
      this.store.dispatch(editQuiz({ quiz: this.quiz }));
    }
    this.router.navigateByUrl('');
  }

  ngOnInit() {
    this.authService.user$.subscribe(user => (this.user = user));
    this.store.pipe(select(selectCurrentQuiz)).subscribe(quiz => {
      this.firstFormGroup = this.formBuilder.group({
        nameControl: ['', Validators.required],
        dateControl: ['', Validators.required],
      });

      this.secondFormGroup = this.formBuilder.group({ teamControl: [''] });

      this.secondFormGroup.controls.teamControl.valueChanges
        .pipe(
          debounceTime(500),

          switchMap(value => this.teamService.getTeams(1, 10, value)),
        )

        .subscribe(teams => (this.allTeams = teams.data));

      this.quiz = quiz;

      if (!!quiz.id) {
        this.isEdit = true;

        this.firstFormGroup = this.formBuilder.group({
          nameControl: [this.quiz.name, Validators.required],
          dateControl: [new Date(this.quiz.startTime), Validators.required],
        });

        this.dataSource = new MatTableDataSource(this.quiz.participants);

        this.minDate = new Date(this.quiz.startTime);
      }
    });
  }

  onNewTeam(name: string): void {
    this.teamService.addTeam(name).subscribe(response => {
      this.quiz.participants.push({
        ...new Participant(new Team(response.id, response.name)),
        watcherId: this.user.id,
      });

      console.log(this.user.id);

      this.dataSource = new MatTableDataSource(this.quiz.participants);
    });
  }

  onExistingTeam(team: Team): void {
    if (!this.quiz.participants.find(p => p.id === team.id)) {
      this.quiz.participants.push({
        ...new Participant(team),
        watcherId: this.user.id,
      });

      console.log(this.user.id);

      this.dataSource = new MatTableDataSource(this.quiz.participants);
    }
  }

  get teamControl(): FormControl {
    return (
      this.secondFormGroup &&
      (this.secondFormGroup.controls.teamControl as FormControl)
    );
  }

  get nameControl(): FormControl {
    return (
      this.firstFormGroup &&
      (this.firstFormGroup.controls.nameControl as FormControl)
    );
  }

  get dateControl(): FormControl {
    return (
      this.firstFormGroup &&
      (this.firstFormGroup.controls.dateControl as FormControl)
    );
  }
  removeParticipant(participant: ParticipantResponse): void {
    this.dataSource.data.splice(
      this.dataSource.data.lastIndexOf(participant),
      1,
    );

    this.dataSource = new MatTableDataSource(this.quiz.participants);

    this.store.dispatch(
      removeParticipantFromQuiz({
        quizId: this.quiz.id,
        participantId: participant.id,
      }),
    );
  }

  addToWatchedTeams(participantId: number) {
    this.quiz.participants.forEach(participant => {
      if (participant.id === participantId) {
        participant.watcherId = this.user.id;
      }
    });
  }

  stopWatching(participantId: number) {
    this.quiz.participants.forEach(participant => {
      if (participant.id === participantId) {
        participant.watcherId = null;
      }
    });
  }

  ngOnDestroy() {
    this.isAlive = false;
  }
}
