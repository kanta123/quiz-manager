// import { quizCardEdit } from './../store/quiz.actions';
import { AppState } from './../../reducers/index';
import { Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { removeSelectedQuiz } from '../store/quiz.actions';

@Injectable()
export class QuizCreateResolver implements Resolve<any> {
  constructor(private store: Store<AppState>) { }
  resolve() {
    this.store.dispatch(removeSelectedQuiz());
  }
}
