import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { AppState } from '../../reducers';
import {
  tap,
  first,
  finalize,
  filter
} from 'rxjs/operators';
import { getQuiz, endedLoading, startedLoading } from '../store/quiz.actions';
import { selectIsLoading } from '../store/quiz.selectors';

@Injectable()
export class QuizEditResolver implements Resolve<any> {
  loading = false;
  QUIZ_ID = 'id';

  constructor(private store: Store<AppState>) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> {
    return this.store.pipe(
      select(selectIsLoading),
      tap(isLoading => {
        if (!this.loading && !isLoading) {
          this.loading = true;

          this.store.dispatch(getQuiz({ quizId: route.params[this.QUIZ_ID] }));

        }
      }),
      filter(isLoading => isLoading),
      first(),
      finalize(() => this.loading = false)
    );
  }
}
