import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { QuizRoutingModule } from './quiz-routing.module';
import { QuizListComponent } from './quiz-list/quiz-list.component';
import { QuizCardComponent } from './quiz-card/quiz-card.component';
import { QuizCreateComponent } from './quiz-create/quiz-create.component';

import { StoreModule } from '@ngrx/store';
import * as fromQuiz from './reducers/quizzes.reducer';
import { EffectsModule } from '@ngrx/effects';

// Material
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { QuizEffects } from './store/quiz.effects';
import { AuthGuardService } from '../auth/guards/auth-guard.service';

@NgModule({
  declarations: [QuizListComponent, QuizCardComponent, QuizCreateComponent],
  imports: [
    CommonModule,
    QuizRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    // Material
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    StoreModule.forFeature(fromQuiz.quizFeatureKey, fromQuiz.quizReducer, {
      metaReducers: fromQuiz.metaReducers,
    }),
    EffectsModule.forFeature([QuizEffects]),
  ],

  providers: [AuthGuardService],
})
export class QuizModule {}
