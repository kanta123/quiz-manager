import { createSelector, createFeatureSelector } from '@ngrx/store';
import { QuizState } from '../reducers/quizzes.reducer';
import * as fromQuizzes from '../reducers/quizzes.reducer';
import { Participant } from 'src/app/models/team/participant';

export const selectQuizState = createFeatureSelector<QuizState>('quiz');

export const selectAllQuizzes = createSelector(
    selectQuizState,
    fromQuizzes.selectAll
);

export const selectPastQuizzes = createSelector(
    selectAllQuizzes,
    quizzes => quizzes.filter(quiz => quiz.startTime < new Date().setHours(0, 0, 0, 0))
);

export const selectUpcomingQuizzes = createSelector(
    selectAllQuizzes,
    quizzes => quizzes.filter(quiz => quiz.startTime >= new Date().setHours(0, 0, 0, 0))
);

export const selectCurrentQuiz = createSelector(
    selectQuizState,
    state => !!state.selectedQuizId ?
        {
            id: state.entities[state.selectedQuizId].id,
            name: state.entities[state.selectedQuizId].name,
            startTime: state.entities[state.selectedQuizId].startTime,
            participants: [...state.entities[state.selectedQuizId].participants]
                .map(participant => ({ ...participant }))
                .sort((first, second) => {
                    if (first.name < second.name) { return -1; }
                    if (first.name > second.name) { return 1; }
                    return 0;
                })
        }

        :

        {
            id: null,
            name: null,
            startTime: null,
            participants: []
        }
);

export const selectIsLoading = createSelector(
    selectQuizState,
    state => state.isLoading
);
