import { createAction, props } from '@ngrx/store';
import { Quiz } from 'src/app/models/quiz/quiz';
import { Update } from '@ngrx/entity';

export const loadUpcomingQuizzes = createAction(
  '[Quiz Resolver] Load 10 Upcoming Quizzes  ',
);

export const upcomingQuizzesLoaded = createAction(
  '[Load Quizzes Effect] 10 Upcoming Quizzes Loaded',
  props<{ quizzes: Quiz[] }>(),
);

export const loadPastQuizzes = createAction(
  '[Quiz Resolver] Load 10 Past Quizzes  ',
);

export const pastQuizzesLoaded = createAction(
  '[Load Quizzes Effect] 10 Past Quizzes Loaded',
  props<{ quizzes: Quiz[] }>(),
);

export const deleteQuiz = createAction(
  '[Quiz List Page] Delete selected quiz',
  props<{ quiz: Quiz }>(),
);

export const quizDeleted = createAction(
  '[Delete Quiz Effect] Quiz deleted',
  props<{ id: number }>(),
);

export const createQuiz = createAction(
  '[Create New Quiz] Create new quiz',
  props<{ quiz: Quiz }>(),
);

export const quizCreated = createAction(
  '[Create New Quiz Effect] New quiz created',
  props<{ quiz: Quiz }>(),
);

export const editQuiz = createAction(
  '[Edit Existing Quiz] Edit quiz',
  props<{ quiz: Quiz }>(),
);

export const quizEdited = createAction(
  '[Edit Existing Quiz Effect] Quiz edited',
  props<{ quiz: Quiz }>(),
);

export const removeParticipantFromQuiz = createAction(
  '[Remove Participant To Quiz] Remove participant',
  props<{ quizId: number; participantId: number }>(),
);

export const participantRemovedFromQuiz = createAction(
  '[Remove Participant To Quiz Effect] Participant removed',
  props<{ participantId: number }>(),
);

export const getQuiz = createAction(
  '[Quiz Card Resolver] Get quiz',
  props<{ quizId: number }>(),
);

export const gotQuiz = createAction(
  '[Quiz Card Resolver Effect] Got quiz',
  props<{ quiz: Quiz }>(),
);

export const addParticipantToQuiz = createAction(
  '[Quiz Create/Edit Component] Add Participant to quiz',
  props<{ partialQuiz: Update<Quiz> }>(),
);

export const endedLoading = createAction(
  '[Loading Indicator] Loading has ended',
);

export const startedLoading = createAction(
  '[Loading Indicator] Loading has started',
);

export const incrementOneParticipantScore = createAction(
  '[Quiz Card View] Increment points on selected participant',
  props<{ quizID: number; participantID: number }>(),
);

export const pointsIncremented = createAction(
  '[Quiz Card View Effect] Points incremented on selected participant',
  props<{ quizId: number; participantId: number; score: number }>(),
);

export const pointsIncrementSuccess = createAction(
  '[Quiz Card View Effect] Points increment success',
);

export const decrementOneParticipantScore = createAction(
  '[Quiz Card View] Decrement points on selected participant',
  props<{ quizID: number; participantID: number }>(),
);

export const pointsDecremented = createAction(
  '[Quiz Card View Effect] Points decremented on selected participant',
  props<{ quizId: number; participantId: number; score: number }>(),
);

export const pointsDecrementSuccess = createAction(
  '[Quiz Card View Effect] Points increment success',
);

export const removeSelectedQuiz = createAction(
  '[Quiz Card View] Remove selected quiz when going from quiz card to quiz create',
);

export const getParticipantScore = createAction(
  '[Quiz Card View] Get the selected participant score from backend',
  props<{ participantId: number }>(),
);
