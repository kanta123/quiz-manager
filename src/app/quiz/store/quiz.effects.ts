import { Injectable } from '@angular/core';
import { createEffect, ofType, Actions } from '@ngrx/effects';
import { QuizActions } from './action-types';
import { QuizService } from 'src/app/service/quiz.service';
import {
  pastQuizzesLoaded,
  upcomingQuizzesLoaded,
  quizCreated,
  quizDeleted,
  quizEdited,
  participantRemovedFromQuiz,
  gotQuiz,
  pointsIncremented,
  pointsDecremented,
  pointsIncrementSuccess,
  pointsDecrementSuccess,
} from './quiz.actions';
import { concatMap, map, flatMap } from 'rxjs/operators';
import { Quiz } from 'src/app/models/quiz/quiz';
import { concat, of } from 'rxjs';
import { ParticipantWebsocketService } from 'src/app/service/websocket.service';

@Injectable()
export class QuizEffects {
  constructor(
    private actions$: Actions,
    private quizService: QuizService,
    private socketService: ParticipantWebsocketService,
  ) {}

  loadUpcomingQuizzes$ = createEffect(() =>
    this.actions$.pipe(
      ofType(QuizActions.loadUpcomingQuizzes),
      concatMap(() =>
        this.quizService.getUpcomingQuizzes(
          1,
          10,
          new Date().setHours(0, 0, 0, 0),
          true,
        ),
      ),
      map(quizzesResponse =>
        upcomingQuizzesLoaded({ quizzes: quizzesResponse.data }),
      ),
    ),
  );

  loadPastQuizzes$ = createEffect(() =>
    this.actions$.pipe(
      ofType(QuizActions.loadPastQuizzes),
      concatMap(() =>
        this.quizService.getUpcomingQuizzes(
          1,
          10,
          new Date().setHours(0, 0, 0, 0),
          false,
        ),
      ),
      map(quizzesResponse =>
        pastQuizzesLoaded({ quizzes: quizzesResponse.data }),
      ),
    ),
  );

  deleteQuiz$ = createEffect(() =>
    this.actions$.pipe(
      ofType(QuizActions.deleteQuiz),
      concatMap(action => this.quizService.deleteQuiz(action.quiz.id)),
      map(id => quizDeleted({ id })),
    ),
  );

  createQuiz$ = createEffect(() =>
    this.actions$.pipe(
      ofType(QuizActions.createQuiz),
      concatMap(action => this.quizService.addQuiz(action.quiz)),
      map(quiz => quizCreated({ quiz })),
    ),
  );

  editQuiz$ = createEffect(() =>
    this.actions$.pipe(
      ofType(QuizActions.editQuiz),
      concatMap(action => this.quizService.updateQuiz(action.quiz)),
      map(quiz => quizEdited({ quiz })),
    ),
  );

  removeParticipant$ = createEffect(() =>
    this.actions$.pipe(
      ofType(QuizActions.removeParticipantFromQuiz),
      concatMap(action =>
        this.quizService.removeParticipant(action.quizId, action.participantId),
      ),
      map(participantId => participantRemovedFromQuiz({ participantId })),
    ),
  );

  getQuiz$ = createEffect(() =>
    this.actions$.pipe(
      ofType(QuizActions.getQuiz),
      concatMap(action => this.quizService.getQuiz(action.quizId)),
      map(quiz => {
        return gotQuiz({ quiz });
      }),
    ),
  );

  incrementPointsOnOneParticipant$ = createEffect(() =>
    this.actions$.pipe(
      ofType(QuizActions.incrementOneParticipantScore),
      flatMap(action =>
        this.quizService.incrementScore(action.quizID, action.participantID),
      ),
      map(() => pointsIncrementSuccess()),
    ),
  );

  decrementPointsOnOneParticipant$ = createEffect(() =>
    this.actions$.pipe(
      ofType(QuizActions.decrementOneParticipantScore),
      flatMap(action =>
        this.quizService.decrementScore(action.quizID, action.participantID),
      ),
      map(() => pointsDecrementSuccess()),
    ),
  );
}
