import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuizListComponent } from './quiz-list/quiz-list.component';
import { QuizCardComponent } from './quiz-card/quiz-card.component';
import { QuizCreateComponent } from './quiz-create/quiz-create.component';
import { QuizzesResolver } from './resolvers/quiz-list.resolver';
import { QuizEditResolver } from './resolvers/quiz-edit.resolver';
import { QuizCreateResolver } from './resolvers/quiz-create.resolver';

const routes: Routes = [
  {
    path: '',
    component: QuizListComponent,
    resolve: {
      quizzes: QuizzesResolver
    }
  },

  {
    path: ':id/info',
    component: QuizCardComponent,
    resolve: {
      info: QuizEditResolver
    }
  },

  {
    path: 'create',
    component: QuizCreateComponent,
    data: { isEdit: false },
    resolve: {
      create: QuizCreateResolver
    }
  },

  {
    path: ':id/edit',
    component: QuizCreateComponent,
    data: { isEdit: true },
    resolve: {
      card: QuizEditResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [QuizzesResolver, QuizEditResolver, QuizCreateResolver]
})
export class QuizRoutingModule { }
