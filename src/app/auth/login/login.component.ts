import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators,
} from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
  ) {
    this.loginFormGroup = this.formBuilder.group({
      emailControl: ['', Validators.required],
      passwordControl: ['', Validators.required],
    });
  }

  public loginFormGroup: FormGroup;

  ngOnInit() {}

  onSubmit() {
    const username = this.loginFormGroup.controls.emailControl.value;
    const password = this.loginFormGroup.controls.passwordControl.value;
    this.authService
      .login(username, password)
      .subscribe(() => this.router.navigateByUrl('/'));
  }
}
