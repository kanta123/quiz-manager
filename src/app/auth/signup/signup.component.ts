import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
  ) {
    this.signupFormGroup = this.formBuilder.group({
      emailControl: ['', Validators.required],
      passwordControl: ['', Validators.required],
      confirmControl: ['', Validators.required],
    });
  }

  public signupFormGroup: FormGroup;

  ngOnInit() {}

  onSubmit() {
    if (this.passwordControl.value !== this.confirmControl.value) {
      console.log('WROONG');
      return;
    }

    if (this.passwordControl.value === '') {
      console.log('WROOOONG AGAIN');
      return;
    }

    this.authService
      .signup(this.emailControl.value, this.passwordControl.value)
      .subscribe(user => console.log(user.username));
  }

  get passwordControl(): FormControl {
    return (
      this.signupFormGroup &&
      (this.signupFormGroup.controls.passwordControl as FormControl)
    );
  }

  get emailControl(): FormControl {
    return (
      this.signupFormGroup &&
      (this.signupFormGroup.controls.emailControl as FormControl)
    );
  }

  get confirmControl(): FormControl {
    return (
      this.signupFormGroup &&
      (this.signupFormGroup.controls.confirmControl as FormControl)
    );
  }
}
